
/* 

Henlo C: 

*/

function getScrollPercent() {
    let scrollTop = $(window).scrollTop();
	let docHeight = $(document).height();
	let winHeight = $(window).height();
	return Math.round(((scrollTop) / (docHeight - winHeight)) * 100);
}


function adjustBackground() {
	let scrollPercent = getScrollPercent();
	console.log(scrollPercent);
	
	if (scrollPercent < 40) {
		document.body.className = "stage-1";
	} else if (scrollPercent < 75) {
		document.body.className = "stage-2";
	} else if (scrollPercent < 96) {
		document.body.className = "stage-3";		
	} else {
		document.body.className = "stage-4";		
	}
}


$(document).ready(function() {
	$(window).scroll(adjustBackground);
});
